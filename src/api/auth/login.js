/**
 * Created by lk on 17/6/4.
 */
import axios from "../../utils/axios";
// 获取信息
export function userInfo(id, token) {
    return axios({
        url: "/auth/userInfo",
        method: "get",
        params: { id, token }
    });
}

export function loginName(username, password) {
    return axios({
        url: "/auth/login",
        method: "post",
        data: { username, password }
    });
}

export function logout(uid, token) {
    return axios({
        url: "/auth/out",
        method: "post",
        data: { uid, token }
    });
}

export function password(data) {
    return axios({
        url: "/auth/password",
        method: "post",
        data: data
    });
}
